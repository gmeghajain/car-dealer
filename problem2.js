// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
//Execute a function to find what the make and model of the last car in the inventory is? 
//Log the make and model into the console in the format of: "Last car is a *car make goes here* *car model goes here*"

let  getLastCarByYear = (data) => {
    if(!data) return[];    
    let latest=0, carDetail;
    for (let i = 0; i < data.length; i++)
    {
        if(data[i]['car_year']>=latest){
            latest=data[i]['car_year'];
            carDetail=data[i];
        } 
    }
    return carDetail;
}
module.exports = getLastCarByYear;
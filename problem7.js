// return all the cars models starting with vowel.

let carModelVowel = (data) =>{
    if(data==null) return NaN;
    let answer=[];
    
    for (let i=0; i<data.length;i++)
       {
         if(data[i]['car_model'].startsWith("A")||data[i]['car_model'].startsWith("E")||data[i]['car_model'].startsWith("I")||data[i]['car_model'].startsWith("O")||data[i]['car_model'].startsWith("U"))
           {
               answer.push(data[i]['car_model']);
           }
        } 
    return answer;
}
module.exports= carModelVowel;


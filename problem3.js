// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
//Execute a function to Sort all the car model names into alphabetical order and 
//log the results in the console as it was returned.
let getCarModelAlphabetically=(data)=>
{
   if(!data) return[];
   return data.sort((a,b)=>{
       let nameA = a.car_model.toUpperCase();
       let nameB = b.car_model.toUpperCase();
       if(nameA<nameB){
           return-1;
       }
       if(nameA>nameB){
           return 1;
       }
       return 0;
   });
};
module.exports = getCarModelAlphabetically;
// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.
//Execute a function and return an array that only contains BMW and Audi cars.
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

module.exports = getBMWAndAudi=(data)=>{
    if(!data) return[];    
    let BMWAndAudi=[];
    let car={};
    for(let i=0;i<data.length;i++)
    {
        if(data[i].car_make==="BMW"||data[i].car_make==="Audi")
        {
            car["id"]=data[i].id;
            car["car_make"]=data[i].car_make;
            car["car_model"]=data[i].car_model;
            car["car_year"]=data[i].car_year;
        }
        BMWAndAudi.push(car);
    }
    return BMWAndAudi;
}


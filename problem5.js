// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
//Using the array you just obtained from the previous problem, find out how many cars were made
//before the year 2000 and return the array of older cars and log its length.
let carOlderThan2000 = (data,yearArray) =>
{
    if(!data) return[];    
    let count=0;
    let newArray=[];
    for(let i=0;i<yearArray.length;i++)
    {
        if(yearArray[i]<2000)
        {
            newArray[count]=(data[i]);
            count++;
        }
    }
    console.log(count+" cars were made before 2000 ");
    return newArray;
}
module.exports = carOlderThan2000;

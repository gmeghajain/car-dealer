// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot.
//Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car.
//Then log the car's year, make, and model in the console log in the format of: 
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const inventory = require("../inventory.js");
const getCarDetailsById = require("../problem1.js");

let temp=getCarDetailsById(inventory, 33);
let x="Car "+ temp['id']+" is a "+temp['car_year']+" "+temp['car_make']+" "+temp['car_model']+".";

console.log(x);
